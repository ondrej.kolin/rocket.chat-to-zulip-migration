import curses

def read_curses_number(msg, cw, min=None, max=None):
    x = None
    if min is not None or max is not None:
        msg += f" range: [{min},{max}]"
    while x is None:
        cw.clear()
        curses.echo()
        cw.addstr(0,0,msg.strip() + ":")
        cw.refresh()
        try:
            x = int(cw.getstr(1, 0, 10))
            if (min is not None and x < min) or (max is not None and x > max):
                x = None
        except ValueError:
            pass
    curses.noecho()
    return x

def read_curses_string(msg, cw, y=0, clear=False):
    MAX_LENGTH = 80
    result = ""
    msg += f" (max_lenght is {MAX_LENGTH})"
    if clear:
        cw.clear()
    curses.echo()
    while result.strip() == "":
        cw.refresh()
        cw.addstr(y, 0, msg)
        result = cw.getstr(y+1, 0, MAX_LENGTH).decode()
    curses.noecho()
    return result

