import texttable as tt
import curses
from settings import ROCKET_CHAT_ROOM_TYPES, RESULTS_PRO_PAGE
import sys
from helpers import read_curses_number


def load_rocketchat_rooms(db):
    rooms = []
    for (key, room) in enumerate(db.rocketchat_room.find()):
        room['local_id'] = key
        rooms.append(room)
    return rooms


def rooms_table(rooms):
    table = tt.Texttable()
    table.header(('#', '_id', 'type', 'name', 'usersCount', 'msgs', 'users'))
    for room in rooms:
        table.add_row((room['local_id'],
                       room['_id'],
                       ROCKET_CHAT_ROOM_TYPES[room['t']],
                       room.get('name'),
                       room.get('usersCount'),
                       room.get('msgs'),
                       ", ".join(room.get('usernames', []))
                       ))
    return table


def draw_table(table: tt.Texttable, cw):
    height, width = cw.getmaxyx()
    table.set_max_width(width)
    table_rows = table.draw().split('\n')
    for (y, row) in enumerate(table_rows):
        # Print the row to the screen
        try:
            cw.addstr(y+1, 0, row)
        except curses.error:
            pass
    return y

def select_room(rooms, cw):
    page = 0
    page_max = len(rooms)//RESULTS_PRO_PAGE
    key = None
    while True:
        cw.clear()
        if key == curses.KEY_DOWN and page > 0:
            page-=1
        elif key == curses.KEY_UP and page < page_max: # Don't go over max pages
            page+=1
        elif key == ord("x"):
            sys.exit(1)
        elif key == ord("s"):
            cw.clear()
            return rooms[read_curses_number("Rocket.chat room #", cw, min=0, max=len(rooms)-1)]
        y = draw_table(rooms_table(rooms[page * RESULTS_PRO_PAGE:(page + 1) * RESULTS_PRO_PAGE]),
                       cw)
        y = y+2 # add some extra space
        cw.addstr(y, 0, f'Key up/down, e_x_it, _s_elect, page: {page}/{page_max}')
        key = cw.getch()

def streams_table(streams):
    table = tt.Texttable()
    table.header(('#', 'stream_id', 'name', 'description[:40]'))
    for stream in streams:
        table.add_row((stream['local_id'],
                       stream['stream_id'],
                       stream.get('name'),
                       stream.get('description', "")[:40]
                       ))
    return table
    pass

def select_stream(zulip_client, cw):
    streams = []
    for (key, stream) in enumerate(zulip_client.get_streams()['streams']):
        stream["local_id"] = key
        streams.append(stream)
    page = 0
    page_max = len(streams) // RESULTS_PRO_PAGE
    key = None
    while True:
        cw.clear()
        if key == curses.KEY_DOWN and page > 0:
            page -= 1
        elif key == curses.KEY_UP and page < page_max:  # Don't go over max pages
            page += 1
        elif key == ord("x"):
            sys.exit(1)
        elif key == ord("s"):
            return streams[read_curses_number("Rocket.chat room #", cw, min=0, max=len(streams) - 1)]
        y = draw_table(streams_table(streams[page * RESULTS_PRO_PAGE:(page + 1) * RESULTS_PRO_PAGE]),
                       cw)
        y = y + 2  # add some extra space
        cw.addstr(y, 0,"Select desired stream for import of the messages\n")
        cw.addstr(y+1, 0, f'Key up/down, e_x_it, _s_elect, page: {page}/{page_max}')
        key = cw.getch()