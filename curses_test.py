# imports
import curses
import texttable as tt
from room_functions import draw_table

table = tt.Texttable()
table.header(('#', '_id'))
rows = (
    (1, "_asdasdasd"),
    (2, "_asdasdasd"),
    (3, "asdasdad"),
)
table.add_rows(rows)

def main(cw):
    page = 0
    page_max = 3
    key = None
    while key != curses.KEY_ENTER:
        cw.clear()
        if key == curses.KEY_DOWN:
            page -= 1
        elif key == curses.KEY_UP:  # Don't go over max pages
            page += 1
        cw.addstr(0, 0, f"PAGINATION ON, last key: {key}, page: {page}/{page_max}")
        key = cw.getch()


curses.wrapper(main)