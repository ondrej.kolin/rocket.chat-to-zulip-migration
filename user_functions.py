import texttable as tt
import zulip
import ldap
from settings import LDAP_BASE_DN, RESULTS_PRO_PAGE
import curses
import sys
from helpers import read_curses_number, read_curses_string

def map_user(user, zulip_client: zulip.Client, ldap_connection: ldap.ldapobject.SimpleLDAPObject):
    """
    Tries to map clients
    Basic rule: one of user e-mails has to be present in zulip, to create the mapping
    :type user: dict
    :type ldap_connection: ldap.ldapobject.SimpleLDAPObject
    :type zulip_client: zulip.Client
    """
    user['mapped_on'] = None
    for email in user['emails']:
        if zulip_client.get_user_presence(email['address'])['result'] == 'success':
            user['mapped_on'] = f'zulip:{email["address"]}'
            return user
        ldap_search = ldap_connection.search_s(LDAP_BASE_DN, ldap.SCOPE_SUBTREE,
                                           f'(|(mail={email["address"]})(gosaMailAlternateAddress={email["address"]}))')
        if len(ldap_search):
            user['mapped_on'] = f'ldap:{ldap_search[0][0]}'
            return user
    return user

def users_table(users):
    table = tt.Texttable()
    table.header(('#', '_id', 'name', 'username', 'emails', 'mapping'))
    for user in users:
        table.add_row((user['local_id'],
                       user['_id'],
                       user.get('name'),
                       user.get('username'),
                       ",".join([email['address'] for email in user['emails']]),
                       str(user.get('mapped_on')).split(":")[0]
                       ))
    return table


def draw_table(table: tt.Texttable, cw, y_offset):
    y = 0
    height, width = cw.getmaxyx()
    table.set_max_width(width)
    table_rows = table.draw().split('\n')
    for (y, row) in enumerate(table_rows):
        # Print the row to the screen
        try:
            cw.addstr(y+y_offset, 0, row)
        except curses.error:
            pass
    return y + y_offset

def edit_user(profile, cw):
    INPUT_LINE = 13
    MESSAGE_LINE = 12
    # Dynamic part
    key = None
    msg = ""
    while True:
        cw.clear()
        cw.addstr(f"Edit user #{profile['local_id']}: \n")
        cw.addstr(f"_id: {profile['_id']}\n"
                  f"name: {profile.get('name')}\n"
                  f"username: {profile.get('username')}\n")
        cw.addstr("emails: " + ",".join([email['address'] for email in profile['emails']]) + "\n")
        cw.addstr(f"Mapping information: \n{profile.get('mapped_on')}\n")
        cw.addstr("----------------\n")
        cw.addstr('_f_inish the edit, map to a _l_dap CN, set the _z_ulip user email address\nmigrate to _d_ummy user, '
                  '_c_reate the user in zulip using the e-mail address and username from Rocket.chat\n')
        if msg != "":
            cw.addstr(MESSAGE_LINE, 0, f"Message: {msg}")
            msg = ""
        # detect action
        key = cw.getch()
        if key == ord("f"):
            return
        if key == ord("l"):
            profile['mapped_on'] = "ldap:" + read_curses_string("Enter the LDAP CN:", cw, INPUT_LINE)
        if key == ord("z"):
            profile['mapped_on'] = "zulip:" + read_curses_string("Enter the LDAP CN:", cw, INPUT_LINE)
        if key == ord("c"):
            profile['mapped_on'] = "create:new"
        if key == ord("d"):
            profile['mapped_on'] = "dummy:user"

def solve_profile_mapping(profiles, cw):
    # Print the table with pagination
    done = False
    page = 0
    page_max = len(profiles) // RESULTS_PRO_PAGE
    key = None
    while True:
        # detect action
        if key == curses.KEY_DOWN and page > 0:
            page -= 1
        elif key == curses.KEY_UP and page < page_max:  # Don't go over max pages
            page += 1
        elif key == ord("x"):
            sys.exit(1)
        elif key == ord("e"):
            user = profiles[read_curses_number("Which user do you want to edit #", cw, min=0, max=len(profiles) - 1)]
            edit_user(user, cw)
        elif key == ord("w"):
            for profile in profiles:
                if profile['mapped_on'] is None:
                    edit_user(profile, cw)
        elif key == ord("f")  and done:
            return
        elif key == ord("d"):
            for i in users_not_mapped:
                profiles[i]['mapped_on'] = "dummy:user"
        # Update mapping information
        users_not_mapped = []
        users_in_ldap = []
        users_in_zulip = []
        users_to_be_created = []
        users_map_to_dummy_user = []
        if not(len(users_not_mapped)):
            done = True
        for profile in profiles:
            if profile['mapped_on'] is None:
                users_not_mapped.append(profile['local_id'])
            elif profile['mapped_on'].startswith("ldap:"):
                users_in_ldap.append(profile['local_id'])
            elif profile['mapped_on'].startswith("zulip:"):
                users_in_zulip.append(profile['local_id'])
            elif profile['mapped_on'].startswith("create:"):
                users_to_be_created.append(profile['local_id'])
            elif profile['mapped_on'].startswith("dummy:"):
                users_map_to_dummy_user.append(profile['local_id'])
        # Render
        cw.clear()
        cw.addstr(f"From {len(profiles)} profiles "
                  f"in zulip {len(users_in_zulip)}, "
                  f"in ldap {len(users_in_ldap)}, "
                  f"{len(users_to_be_created)} will get new account created, "
                  f"{len(users_map_to_dummy_user)} will be mapped to dummy user, "
                  f"{len(users_not_mapped)} is not mapped yet\n")
        cw.addstr(f'notice: The way NoSQL databases work, '
                  f'some user profiles may appear multiple times, they will be merged\n')
        if len(users_not_mapped):
            cw.addstr(f'# waiting to be mapped ' + ", ".join([ str(_) for _ in users_not_mapped]) + "\n")
        y = draw_table(users_table(profiles[page * RESULTS_PRO_PAGE:(page + 1) * RESULTS_PRO_PAGE]),
                       cw,
                       3) # Protect the top text from getting overwritten
        y = y + 2  # Offset from the table
        cw.addstr(y, 0, 'Key up/down, e_x_it the program, _e_dit user, '
                        'map all non-mapped users _d_ummy, _w_alk through non-assigned users, ')
        if not len(users_not_mapped):
            cw.addstr("_f_inish mapping")
        cw.addstr(f', page: {page}/{page_max}')
        key = cw.getch()
