from settings import ZULIP_REALM_PK, ZULIP_DUMMY_USER_ID, MESSAGE_IMPORT_WORKERS
import ldap
/home/ondrej/zulip/zerver/lib/create_user.py

zulip_users_cache = {"filled": False, "users": {}}
rocket_chat_id_to_zulip_account_cache = {}


def zulip_create_user(email, short_name, full_name):
    try:
        return UserProfile.objects.get(email=email)
    except IndexError:
        pass
    return create_user("tesat@benocs.com", None, Realm.objects.get(pk=ZULIP_REALM_PK), full_name, short_name)


# Zulip, LDAP, Create, Dummy
def get_or_create_zulip_account(user, ldap_connection):
    global rocket_chat_id_to_zulip_account_cache
    if user['_id'] in rocket_chat_id_to_zulip_account_cache:
        return rocket_chat_id_to_zulip_account_cache[user['_id']]
    account = None
    if user['mapped_on'].startswith("zulip"):
        account = UserProfile.objects.get(email=user['mapped_on'].split(":")[1])
        if account is None:
            raise Exception("This should never happen! User not found in zulip's cache with email")
    elif user['mapped_on'].startswith("ldap:"):
        try:
            ldap_res = ldap_connection.search_s(user['mapped_on'].split(":")[1], ldap.SCOPE_BASE)[0]
        except ldap.NO_SUCH_OBJECT:
            raise Exception(f"Did not found the DN {user['mapped_on'].split(':')[1]} in the LDAP directory")
        email = ldap_res[1]['mail'][0].decode()
        short_name = ldap_res[1]['uid'][0].decode()
        full_name = ldap_res[1]['cn'][0].decode()
        account = zulip_create_user(email, short_name, full_name)
    elif user['mapped_on'].startswith("create:"):
        try:
            account = zulip_create_user(user['emails'][0]['address'],user['username'], user['name'])
        except KeyError:
            raise Exception(f"user {user['_id']} {user['name']} does not have an email address")
    elif user['mapped_on'].startswith("dummy:"):
        account = UserProfile.objects.get(pk=ZULIP_DUMMY_USER_ID)
    if account == None:
        raise Exception(f"Unable to get or create an account for {user}")
    rocket_chat_id_to_zulip_account_cache[user['_id']] = account
    return account


def create_user_mapping(users, ldap_connection):
    global rocket_chat_id_to_zulip_account_cache
    for user in users:
        # Test if the user exists
        account = get_or_create_zulip_account(user, ldap_connection)
        user['account'] = account
    return rocket_chat_id_to_zulip_account_cache

def migrate_attachment(rc_message):

def migrate_mesage(rc_message):
    # if (has_attachment(rc_message)):
        # if (is_local_attachment):
            # upload attachment to zulip
    # GENERATE attachment_link markdown

def migrate_messages(messages_cursor, rocket_id_to_zulip_id_mapping, zulip_room, zulip_topic):
    for message in messages_cursor:
        migrate_mesage(message)
        # get the message
        # download the attachment into tmp
        # upload the attachment
        # push the message into zulip realm, room, topic
        # add the markdown link to the content
        #




